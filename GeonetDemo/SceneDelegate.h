//
//  SceneDelegate.h
//  GeonetDemo
//
//  Created by Mac on 27/12/19.
//  Copyright © 2019 ToukirNaim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

