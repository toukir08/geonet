//
//  DashBoardViewController.h
//  GeonetDemo
//
//  Created by Mac on 28/12/19.
//  Copyright © 2019 ToukirNaim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "Config.h"

NS_ASSUME_NONNULL_BEGIN

@interface DashBoardViewController : UIViewController
{
    NSString *userName;
    UIView *nameView;
    UILabel *nameLabel;
    UILabel *overviewLabel;
    
    UIImageView* feature1ImageView;
    UIImageView* feature2ImageView;
    
    UILabel *feature1Label;
    UILabel *feature2Label;
    UILabel *feature3Label;
    
    UIImageView* iconImageView;
    UILabel *urlLabel;
    
    UIButton *logoutButton;
}
-(void)setUserInfo:(NSString*)username;
@end

NS_ASSUME_NONNULL_END
