//
//  ViewController.h
//  GeonetDemo
//
//  Created by Mac on 27/12/19.
//  Copyright © 2019 ToukirNaim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Config.h"

@interface ViewController : UIViewController
{
    UIImageView* logoImageView;
    UITextField* userTextField;
    UITextField* passTextField;
    UIButton* loginButton;
    UIButton* forgetPasswordButton;
    UIButton* newSignUpButton;
    
    UIImageView* userTextFieldImage;
    UIImageView* passTextFieldImage;
    
    UIColor* customColor1;
    UIColor* customColor2;
}


@end

