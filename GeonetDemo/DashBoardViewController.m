//
//  DashBoardViewController.m
//  GeonetDemo
//
//  Created by Mac on 28/12/19.
//  Copyright © 2019 ToukirNaim. All rights reserved.
//

#import "DashBoardViewController.h"

@interface DashBoardViewController ()

@end

@implementation DashBoardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = RGB(68.0, 195.0, 251.0);
    [self setNameView];
    [self setUIProperty];
    NSLog(@"name: %@", userName);
}
//set user name view and posiitons for different devices
-(void)setNameView
{
    nameView = [[UIView alloc] init];
    nameView.layer.masksToBounds = YES;
    //nameView.layer.cornerRadius = 5;
    //nameView.translatesAutoresizingMaskIntoConstraints = NO;
    
    nameView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:nameView];
    
    nameLabel = [[UILabel alloc] init];
    nameLabel.backgroundColor = [UIColor clearColor];
    nameLabel.textAlignment = NSTextAlignmentCenter;
    nameLabel.textColor = RGB(68.0, 195.0, 251.0);
    nameLabel.numberOfLines = 0;
    //label.lineBreakMode = UILineBreakModeWordWrap;
    nameLabel.text = userName;
    [nameView addSubview:nameLabel];
    
    if (IS_IPAD)
    {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenRect.size.width;
        CGFloat screenHeight = screenRect.size.height;
        
        nameView.frame = CGRectMake(0,80,200,40);
        nameLabel.frame = CGRectMake(0, 1, 200, 38);
        [nameLabel setFont:[UIFont systemFontOfSize:38]];
        
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:nameView.bounds byRoundingCorners:UIRectCornerTopRight|UIRectCornerBottomRight cornerRadii:CGSizeMake(10.0, 10.0)];

               CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
               maskLayer.frame = nameView.bounds;
               maskLayer.path = maskPath.CGPath;
               nameView.layer.mask = maskLayer;
    }
    else
    {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenRect.size.width;
        CGFloat screenHeight = screenRect.size.height;
        
        nameView.frame = CGRectMake(0,50,100,20);
        nameLabel.frame = CGRectMake(0, 1, 100, 18);
        [nameLabel setFont:[UIFont systemFontOfSize:18]];
        
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:nameView.bounds byRoundingCorners:UIRectCornerTopRight|UIRectCornerBottomRight cornerRadii:CGSizeMake(5.0, 5.0)];

               CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
               maskLayer.frame = nameView.bounds;
               maskLayer.path = maskPath.CGPath;
               nameView.layer.mask = maskLayer;
    }
    
    
}
// set all ui elements and positions for different devices
-(void)setUIProperty
{
    logoutButton = [[UIButton alloc] init];
    [logoutButton setBackgroundColor:RGB(243.0, 160.0, 67.0)];
    logoutButton.layer.cornerRadius = 10;
    logoutButton.clipsToBounds = YES;
    [logoutButton setTitle:@"logout" forState:UIControlStateNormal];
    [logoutButton addTarget:self action:@selector(logoutAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:logoutButton];
    
    overviewLabel = [[UILabel alloc] init];
    //overviewLabel.backgroundColor = [UIColor clearColor];
    overviewLabel.textAlignment = NSTextAlignmentLeft;
    overviewLabel.textColor = [UIColor whiteColor];
    overviewLabel.numberOfLines = 0;
    //label.lineBreakMode = UILineBreakModeWordWrap;
    overviewLabel.text = @"Overview";
    [self.view addSubview:overviewLabel];
    
    feature1Label = [[UILabel alloc] init];
    feature1Label.textAlignment = NSTextAlignmentLeft;
    feature1Label.textColor = [UIColor whiteColor];
    feature1Label.numberOfLines = 0;
    //label.lineBreakMode = UILineBreakModeWordWrap;
    feature1Label.text = @"> Premium Voice Quality";
    [self.view addSubview:feature1Label];
    
    feature2Label = [[UILabel alloc] init];
    feature2Label.textAlignment = NSTextAlignmentLeft;
    feature2Label.textColor = [UIColor whiteColor];
    feature2Label.numberOfLines = 0;
    //label.lineBreakMode = UILineBreakModeWordWrap;
    feature2Label.text = @"> Online Control Panel";
    [self.view addSubview:feature2Label];
    
    feature3Label = [[UILabel alloc] init];
    feature3Label.textAlignment = NSTextAlignmentLeft;
    feature3Label.textColor = [UIColor whiteColor];
    feature3Label.numberOfLines = 0;
    //label.lineBreakMode = UILineBreakModeWordWrap;
    feature3Label.text = @"> Recharge by Phone";
    [self.view addSubview:feature3Label];
    
    iconImageView =[[UIImageView alloc] init];
    iconImageView.image=[UIImage imageNamed:@"icon.png"];
    iconImageView.contentMode = UIViewContentModeScaleToFill;
    [self.view addSubview:iconImageView];
    
    urlLabel = [[UILabel alloc] init];
    urlLabel.textAlignment = NSTextAlignmentCenter;
    urlLabel.textColor = RGB(243.0, 160.0, 67.0);
    urlLabel.numberOfLines = 0;
    //label.lineBreakMode = UILineBreakModeWordWrap;
    urlLabel.text = @"www.gogeonet.com";
    [self.view addSubview:urlLabel];
    
    if (IS_IPAD)
    {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenRect.size.width;
        CGFloat screenHeight = screenRect.size.height;
        
        logoutButton.frame = CGRectMake(screenWidth-110, 80,
                                       100, 40);
        logoutButton.titleLabel.font = [UIFont systemFontOfSize:22];
        
        overviewLabel.frame = CGRectMake(5, 150, screenWidth, 80);
        [overviewLabel setFont:[UIFont systemFontOfSize:80]];
        
        feature1Label.frame = CGRectMake(-screenWidth, 250, screenWidth, 60);
        [feature1Label setFont:[UIFont systemFontOfSize:50]];
        
        feature2Label.frame = CGRectMake(-screenWidth, 330, screenWidth, 60);
        [feature2Label setFont:[UIFont systemFontOfSize:50]];
        
        feature3Label.frame = CGRectMake(-screenWidth, 420, screenWidth, 60);
        [feature3Label setFont:[UIFont systemFontOfSize:50]];
        
        iconImageView.frame = CGRectMake(screenWidth/2-80,screenHeight-200,160,140);
        urlLabel.frame = CGRectMake(0,screenHeight-60,screenWidth,30);
        [urlLabel setFont:[UIFont systemFontOfSize:30]];
    }
    else
    {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenRect.size.width;
        CGFloat screenHeight = screenRect.size.height;

        logoutButton.frame = CGRectMake(screenWidth-60, 50,
                                      50, 20);
        logoutButton.titleLabel.font = [UIFont systemFontOfSize:12];

        overviewLabel.frame = CGRectMake(5, 100, screenWidth, 40);
        [overviewLabel setFont:[UIFont systemFontOfSize:40]];

        feature1Label.frame = CGRectMake(-screenWidth, 150, screenWidth, 30);
        [feature1Label setFont:[UIFont systemFontOfSize:30]];

        feature2Label.frame = CGRectMake(-screenWidth, 200, screenWidth, 30);
        [feature2Label setFont:[UIFont systemFontOfSize:30]];

        feature3Label.frame = CGRectMake(-screenWidth, 250, screenWidth, 30);
        [feature3Label setFont:[UIFont systemFontOfSize:30]];

        iconImageView.frame = CGRectMake(screenWidth/2-80,screenHeight-200,160,140);
        urlLabel.frame = CGRectMake(0,screenHeight-60,screenWidth,30);
        [urlLabel setFont:[UIFont systemFontOfSize:30]];
    }
    
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self nameViewAnimation];
}
//set animation for overview features
-(void)nameViewAnimation
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    [UIView animateWithDuration:2.0
         delay:0
       options:UIViewAnimationOptionCurveEaseOut
    animations:^{
        if(IS_IPAD)
            feature1Label.frame = CGRectMake(5, 250, screenWidth, 60);
        else
            feature1Label.frame = CGRectMake(5, 150, screenWidth, 30);
            
        }completion:^(BOOL finished){
                                [UIView animateWithDuration:2.0
                                     delay:0
                                   options:UIViewAnimationOptionCurveEaseOut
                                animations:^{
                                    if(IS_IPAD)
                                        feature2Label.frame = CGRectMake(5, 330, screenWidth, 60);
                                    else
                                        feature2Label.frame = CGRectMake(5, 200, screenWidth, 30);
                                    }completion:^(BOOL finished){
                                                            [UIView animateWithDuration:2.0
                                                                 delay:0
                                                               options:UIViewAnimationOptionCurveEaseOut
                                                            animations:^{
                                                                if(IS_IPAD)
                                                                    feature3Label.frame = CGRectMake(5, 420, screenWidth, 60);
                                                                else
                                                                    feature3Label.frame = CGRectMake(5, 250, screenWidth, 30);
                                                                }completion:^(BOOL finished){
                                                                                        
                                                                                        
                                                                                    }];
                                                            
                                                        }];
                                
                            }];
}
//get username
-(void)setUserInfo:(NSString*)username
{
    userName = username;
}
//logout button action
- (void)logoutAction:(UIButton*)button
{
    [Config sharedInstance].pageStatus = login;
     [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
