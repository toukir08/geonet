//
//  ViewController.m
//  GeonetDemo
//
//  Created by Mac on 27/12/19.
//  Copyright © 2019 ToukirNaim. All rights reserved.
//

#import "ViewController.h"
#import "DashBoardViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setBasicProperty];
    [self intializeUIElemets];
    [self uiElementsSetPosition];
    
    //get event for hiding keyboard
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
}
//Hide keyboard after entering text on texfield
-(void)dismissKeyboard
{
    [userTextField resignFirstResponder];
    [passTextField resignFirstResponder];
}
//set custom colors
- (void)setBasicProperty
{
    [Config sharedInstance].pageStatus = login;
    customColor1 = RGB(68.0, 195.0, 251.0);
    customColor2 = RGB(243.0, 160.0, 67.0);
}
//initialize ui elements
- (void)intializeUIElemets
{
    logoImageView =[[UIImageView alloc] init];
    logoImageView.image=[UIImage imageNamed:@"logo.png"];
    logoImageView.contentMode = UIViewContentModeScaleToFill;
    [self.view addSubview:logoImageView];
    
    userTextField = [[UITextField alloc] init];
    userTextField.placeholder = @"  Phone / E-mail";
    [userTextField setTextColor:customColor1];
    [self.view addSubview:userTextField];
    userTextFieldImage=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [userTextFieldImage setImage:[UIImage imageNamed:@"user.png"]];
    [userTextFieldImage setContentMode:UIViewContentModeCenter];
    userTextField.leftView=userTextFieldImage;
    userTextField.layer.borderColor=[[UIColor grayColor]CGColor];
    userTextField.layer.borderWidth= 1.0f;
    userTextField.leftViewMode=UITextFieldViewModeAlways;
    
    passTextField = [[UITextField alloc]init ];
    passTextField.placeholder = @"  *********";
    [passTextField setTextColor:customColor1];
    passTextField.secureTextEntry = YES;
    [self.view addSubview:passTextField];
    passTextFieldImage=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [passTextFieldImage setImage:[UIImage imageNamed:@"pass.png"]];
    [passTextFieldImage setContentMode:UIViewContentModeCenter];
    passTextField.leftView=passTextFieldImage;
    passTextField.layer.borderColor=[[UIColor grayColor]CGColor];
    passTextField.layer.borderWidth= 1.0f;
    passTextField.leftViewMode=UITextFieldViewModeAlways;
    
    loginButton = [[UIButton alloc] init];
    [loginButton setBackgroundColor:customColor1];
    [loginButton setTitle:@"Let's Go!" forState:UIControlStateNormal];
    [loginButton addTarget:self action:@selector(loginAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:loginButton];
    
    forgetPasswordButton = [[UIButton alloc] init];
    [forgetPasswordButton setBackgroundColor:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0]];
    [forgetPasswordButton setTitleColor:customColor2 forState:UIControlStateNormal];
    [forgetPasswordButton setTitle:@"Forget password?" forState:UIControlStateNormal];
    [forgetPasswordButton addTarget:self action:@selector(forgetPasswordButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    forgetPasswordButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self.view addSubview:forgetPasswordButton];
    
    newSignUpButton = [[UIButton alloc] init];
    [newSignUpButton setBackgroundColor:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0]];
    [newSignUpButton setTitleColor:customColor2 forState:UIControlStateNormal];
    [newSignUpButton setTitle:@"New? Sign Up!" forState:UIControlStateNormal];
    [newSignUpButton addTarget:self action:@selector(newSignUpButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    newSignUpButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [self.view addSubview:newSignUpButton];
    
}
//set positions for ui elements based on different iphone and ipad sizes
- (void)uiElementsSetPosition
{
    if (IS_IPAD)
    {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenRect.size.width;
        CGFloat screenHeight = screenRect.size.height;
        CGFloat startPoint = screenHeight/4;
        CGFloat aspactRatioLogo = 373/132;
        CGFloat logoHeight = (screenWidth - 150) / aspactRatioLogo;
        
        logoImageView.frame = CGRectMake(75,startPoint,screenWidth - 150,logoHeight);
        userTextField.frame = CGRectMake(75, startPoint+logoHeight+20, screenWidth-150, 80);
        [userTextField setFont:[UIFont systemFontOfSize:30]];
        userTextFieldImage.frame = CGRectMake(0, 0, 60, 60);
        
        passTextField.frame = CGRectMake(75, startPoint+logoHeight+80+40,
                                         screenWidth-150, 80);
        [passTextField setFont:[UIFont systemFontOfSize:30]];
        passTextFieldImage.frame = CGRectMake(0, 0, 60, 60);
        loginButton.frame = CGRectMake(75, startPoint+logoHeight+80+40+10+80,
                                       screenWidth-150, 70);
        loginButton.titleLabel.font = [UIFont systemFontOfSize:30];
        forgetPasswordButton.frame = CGRectMake(75, startPoint+logoHeight+80+50+80+70+10,
                                                (screenWidth-150)/2, 22);
        forgetPasswordButton.titleLabel.font = [UIFont systemFontOfSize:25];
        newSignUpButton.frame = CGRectMake((screenWidth-150)/2 +75 , startPoint+logoHeight+80+50+80+70+10,
                                                           (screenWidth-150)/2, 20);
        newSignUpButton.titleLabel.font = [UIFont systemFontOfSize:25];
    }
    else
    {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenRect.size.width;
        CGFloat screenHeight = screenRect.size.height;
        CGFloat startPoint = screenHeight/4;
        CGFloat aspactRatioLogo = 373/132;
        CGFloat logoHeight = (screenWidth - 50) / aspactRatioLogo;
        
        logoImageView.frame = CGRectMake(25,startPoint,screenWidth - 50,logoHeight);
        userTextField.frame = CGRectMake(25, startPoint+logoHeight+10, screenWidth-50, 60);
        userTextFieldImage.frame = CGRectMake(0, 0, 25, 25);
        passTextField.frame = CGRectMake(25, startPoint+logoHeight+60+20,
                                         screenWidth-50, 60);
        passTextFieldImage.frame = CGRectMake(0, 0, 25, 25);
        loginButton.frame = CGRectMake(25, startPoint+logoHeight+60+20+10+60,
                                       screenWidth-50, 50);
        forgetPasswordButton.frame = CGRectMake(25, startPoint+logoHeight+60+30+60+50+10,
                                                (screenWidth-50)/2, 20);
        forgetPasswordButton.titleLabel.font = [UIFont systemFontOfSize:15];
        newSignUpButton.frame = CGRectMake((screenWidth-50)/2+25, startPoint+logoHeight+60+30+60+50+10,
                                                           (screenWidth-50)/2, 20);
        newSignUpButton.titleLabel.font = [UIFont systemFontOfSize:15];
        
    }
}
//login Button action
- (void)loginAction:(UIButton*)button
 {
     NSString *userInfo = [userTextField text];
     if([userInfo  isEqual: @""])
         userInfo = @"TestUser";
     DashBoardViewController * dashBoardViewController = [[DashBoardViewController alloc] init];
     [Config sharedInstance].pageStatus = dashboard;
     [dashBoardViewController setUserInfo:userInfo];
     dashBoardViewController.modalPresentationStyle = UIModalPresentationCustom;
     [self presentViewController:dashBoardViewController animated:YES completion:nil];
}
//forgetPasswordButton action
- (void)forgetPasswordButtonAction:(UIButton*)button
{
    NSLog(@"Forget Password Action");
}
//newSignUpButton action
- (void)newSignUpButtonAction:(UIButton*)button
{
     NSLog(@"New Users Action");
}


@end
