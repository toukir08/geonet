//
//  Config.h
//  GeonetDemo
//
//  Created by Mac on 27/12/19.
//  Copyright © 2019 Toukir Naim. All rights reserved.
//

#import <Foundation/Foundation.h>

#define RGB(r,g,b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0]

#define IS_IPAD ([[UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPad)

enum PageStatus
{
    login,
    dashboard
};

@interface Config : NSObject
@property (nonatomic,assign) enum PageStatus pageStatus;
+(Config *)sharedInstance;

@end
